<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "email".
 *
 * @property int $ide
 * @property string $email
 * @property int $idf
 *
 * @property Fotografo $f
 */
class Email extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'email';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['idf'], 'integer'],
            [['email'], 'string', 'max' => 127],
            [['idf'], 'exist', 'skipOnError' => true, 'targetClass' => Fotografo::className(), 'targetAttribute' => ['idf' => 'idf']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'ide' => 'Ide',
            'email' => 'Email',
            'idf' => 'Idf',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getF()
    {
        return $this->hasOne(Fotografo::className(), ['idf' => 'idf']);
    }
}
