<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Ne */

$this->title = 'Update Ne: ' . $model->idne;
$this->params['breadcrumbs'][] = ['label' => 'Nes', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->idne, 'url' => ['view', 'id' => $model->idne]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="ne-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
