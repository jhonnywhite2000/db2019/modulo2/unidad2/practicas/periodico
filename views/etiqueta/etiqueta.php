<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Etiquetas';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="etiqueta-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'nombre',
            'noticias',
            'autores',
            [
            'label'=>'Ver noticias',
            'format'=>'raw',
            'content'=>function($model){
                return Html::a('Ver noticias', ['etiqueta/noticiasetiqueta', 'id'=>$model['ide']],
                        ['class'=>'']
            );
            }     
            ], 
                    
        ],
    ]); ?>


</div>
